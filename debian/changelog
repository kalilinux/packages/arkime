arkime (5.6.1-0kali1) kali-dev; urgency=medium

  * New upstream version 5.6.1

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 24 Feb 2025 18:19:21 +0100

arkime (5.6.0-0kali1) kali-dev; urgency=medium

  * New upstream version 5.6.0
  * Refresh patches
  * Use new variable ARKIME_BUILD_FULL_VERSION in debian/rules
  * Bump Standards-Version to 4.7.0 (no changes)

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 16 Jan 2025 12:07:49 +0100

arkime (5.5.0-0kali1) kali-dev; urgency=medium

  * New upstream version 5.5.0
  * Refresh patches
  * Add a patch to fix the build

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 21 Nov 2024 14:52:35 +0100

arkime (5.4.0-0kali2) kali-dev; urgency=medium

  * Install systemd unit in /usr/lib instead of /lib

 -- Arnaud Rebillout <arnaudr@kali.org>  Thu, 12 Sep 2024 20:41:22 +0700

arkime (5.4.0-0kali1) kali-dev; urgency=medium

  * New upstream version 5.4.0

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 20 Aug 2024 09:45:22 +0200

arkime (5.3.0-0kali1) kali-dev; urgency=medium

  * New upstream version 5.3.0
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 04 Jul 2024 19:05:33 +0200

arkime (5.2.0-0kali1) kali-dev; urgency=medium

  * New upstream version 5.2.0
  * Refresh patches
  * Update diebna/rules to use node >= 18

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 29 May 2024 17:54:31 +0200

arkime (5.1.1-0kali1) kali-dev; urgency=medium

  * New upstream version 5.1.1

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 18 Apr 2024 17:32:35 +0200

arkime (5.0.1-0kali2) kali-dev; urgency=medium

  * Adapt arkime-parliament and autopkgtest

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 08 Mar 2024 11:38:13 +0100

arkime (5.0.1-0kali1) kali-dev; urgency=medium

  * New upstream version 5.0.1
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 06 Mar 2024 11:10:20 +0100

arkime (4.6.0-0kali1) kali-dev; urgency=medium

  * New upstream version 4.6.0
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 17 Oct 2023 09:38:31 +0200

arkime (4.5.0-0kali1) kali-dev; urgency=medium

  [ Steev Klimaszewski ]
  * Add xdg-utils to test dependencies.
  * Fix previous commit because it's Depends.

  [ Sophie Brun ]
  * New upstream version 4.5.0

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 14 Sep 2023 11:43:17 +0200

arkime (4.4.0-0kali1) kali-dev; urgency=medium

  * New upstream version 4.4.0
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 10 Aug 2023 11:23:09 +0200

arkime (4.3.2-0kali1) kali-dev; urgency=medium

  * New upstream version 4.3.2
  * Bump Standards-Version to 4.6.2

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 15 Jun 2023 13:50:25 +0200

arkime (4.3.1-0kali1) kali-dev; urgency=medium

  * New upstream version 4.3.1

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 09 May 2023 15:34:02 +0200

arkime (4.3.0-0kali1) kali-dev; urgency=medium

  * CI: disable reprotest
  * Fix logrotate if log dir does not exist
  * Fix lintian-overrides
  * Fix lintian-overrides
  * New upstream version 4.3.0

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 28 Apr 2023 09:58:29 +0200

arkime (4.2.0-0kali1) kali-dev; urgency=medium

  [ Steev Klimaszewski ]
  * ci: Don't build on 32bit architectures.
  * Fix autopkgtest script path
  * Make autopkgtest script executable.
  * Fix autopkgtest script to call correct scripts.
  * Allow autopkgtest to output to stderr

  [ Sophie Brun ]
  * New upstream version 4.2.0
  * Refresh patches
  * Update lintian-overrides

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 18 Apr 2023 16:17:30 +0200

arkime (4.0.3-0kali2) kali-dev; urgency=medium

  * Add logrotate for logs files

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 16 Jan 2023 16:09:03 +0100

arkime (4.0.3-0kali1) kali-dev; urgency=medium

  * Initial release. (see 8073)

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 09 Jan 2023 17:06:19 +0100
